<?php

namespace App\Repositories;

// use App\Exceptions\CreateCarouselErrorException;
use Illuminate\Database\QueryException;
use App\WeightLog;
use DB;
use Validator;

class WeightLogRepository
{
    /**
     * WeightLogRepository constructor.
     * @param WeightLog $weightlog
     */
    public function __construct(WeightLog $weightlog)
    {
        $this->model = $weightlog;
    }

    /**
     * @return Illuminate\Database\Eloquent\Collection 
     */
    public function get() : \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->orderBy('log_date', 'DESC')->get();
    }

    /**
     * @param int $id
     * @return WeightLog
     */
    public function findById(int $id) : WeightLog
    {
        return $this->model->find($id);
    }

    /**
     * @return WeightLog
     */
    public function getStats() : WeightLog
    {
        return $this->model
                ->select(DB::raw(
                    'SUM(max) AS max,
                    SUM(min) AS min,
                    SUM(variance) AS variance,
                    COUNT(DISTINCT id) AS counted'))
                ->first();
    }
    
    /**
     * @param array $data
     * @return WeightLog
     */
    public function create(array $data) : WeightLog
    {
        try {
            $data['variance'] = $data['max'] - $data['min'];
            return $this->model->create($data);
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function update(array $data) : array
    {
        try {
            $data['variance'] = $data['max'] - $data['min'];
            $this->model->update($data);
            return $data;
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete(int $id) : int
    {
        try {
            return $this->model->destroy($id);
        } catch (QueryException $e) {
            dd($e->getMessage());
        }
    }
}
