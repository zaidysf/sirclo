<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\WeightLog;
$factory->define(App\WeightLog::class, function (Faker $faker) {
    return [
        'log_date' => '2018-10-01',
        'min' => 5,
        'max' => 10,
        'variance' => 5
    ];
});
