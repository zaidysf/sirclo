<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WeightLogRepository;
use Validator;

class WeightLogController extends Controller
{
    /**
     * WeightLogController constructor.
     * @param WeightLogRepository $repo
     */
    public function __construct(WeightLogRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index()
    {
    	$weight_logs = $this->repo->get();
    	$weight_stats = $this->repo->getStats();
        return view('weight_log.index',compact('weight_logs', 'weight_stats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('weight_log.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
            'log_date' => 'required|date',
	        'max' => 'required|numeric|min:'.$request->min,
	        'min' => 'required|numeric|max:'.$request->max
        ]);

        if ($validator->fails()) {
            return redirect()
            			->route('weight-log.create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $request->request->add(['variance' => $request->max - $request->min]);
        
        $this->repo->create($request->all());
        return redirect()
        			->route('weight-log.index')
                	->with('success','WeightLog created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $weight_log = $this->repo->findById($id);
        return view('weight_log.show',compact('weight_log'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $weight_log = $this->repo->findById($id);
        return view('weight_log.edit',compact('weight_log'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'log_date' => 'required|date',
	        'max' => 'required|numeric|min:'.$request->min,
	        'min' => 'required|numeric|max:'.$request->max
        ]);

        if ($validator->fails()) {
            return redirect()
            			->route('weight-log.edit', $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->repo->update($request->all());
        return redirect()->route('weight-log.index')
                        ->with('success','WeightLog updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);
        return redirect()->route('weight-log.index')
                        ->with('success','WeightLog deleted successfully');
    }
}
