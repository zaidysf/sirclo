<?php

namespace App\Http\Controllers;

class CartController extends Controller
{
    public function index()
    {
        $cart = new \App\Cart;
        $cart->addProduct("Topi Putih", 2);
        $cart->addProduct("Kemeja Hitam", 3);
        $cart->addProduct("Sepatu Merah", 1);
        $cart->addProduct("Sepatu Merah", 4);
        $cart->addProduct("Sepatu Merah", 2);
        $cart->removeProduct("Kemeja Hitam");
        $cart->removeProduct("Baju Hijau");
        return $cart->showCart();
    }
}
