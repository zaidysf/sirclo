<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class WeightLog extends Model
{
    protected $fillable = [
        'log_date', 'max', 'min', 'variance'
    ];
}
