<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\WeightLog;
use App\Repositories\WeightLogRepository;

class WeightLogTest extends TestCase
{
    /** @test */
    public function canReadWeightLog()
    {
        $weightLog = factory(WeightLog::class)->create();
        $weightLogRepo = new WeightLogRepository(new WeightLog);
        $found = $weightLogRepo->findById($weightLog->id);
        
        $this->assertInstanceOf(WeightLog::class, $found);
        $this->assertEquals($found['log_date'], $weightLog->log_date);
        $this->assertEquals($found['min'], $weightLog->min);
        $this->assertEquals($found['max'], $weightLog->max);
    }

    /** @test */
    public function canCreateWeightLog()
    {
        $data = [
            'log_date' => '2018-10-01',
            'min' => 5,
            'max' => 10,
        ];
      
        $weightLogRepo = new WeightLogRepository(new WeightLog);
        $weightLog = $weightLogRepo->create($data);
      
        $this->assertInstanceOf(WeightLog::class, $weightLog);
        $this->assertEquals($data['log_date'], $weightLog->log_date);
        $this->assertEquals($data['min'], $weightLog->min);
        $this->assertEquals($data['max'], $weightLog->max);
    }

    /** @test */
    public function canUpdateWeightLog()
    {
        $data = [
            'log_date' => '2018-10-01',
            'min' => 5,
            'max' => 10,
        ];
      
        $weightLogRepo = new WeightLogRepository(new WeightLog);
        $weightLog = $weightLogRepo->update($data);
      
        $this->assertEquals($data['log_date'], $weightLog['log_date']);
        $this->assertEquals($data['min'], $weightLog['min']);
        $this->assertEquals($data['max'], $weightLog['max']);
    }
    
    /** @test */
    public function canDeleteWeightLog()
    {
        $weightLog = factory(WeightLog::class)->create();
      
        $weightLogRepo = new WeightLogRepository($weightLog);
        $delete = $weightLogRepo->delete($weightLog->id);
        
        $this->assertEquals(1, $delete);
    }
}
